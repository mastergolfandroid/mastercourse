/**
 * 更新app
 * 注意修改filename及urlStr
 */
package com.teewell.samsunggolfmate.utils;

import java.io.File;
import java.io.IOException;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.DownloadManager;
import android.app.DownloadManager.Request;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.webkit.MimeTypeMap;

import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.master.mastercourse.R;
import com.teewell.samsunggolfmate.activities.MyApplication;
import com.teewell.samsunggolfmate.beans.ServerAppBean;
import com.teewell.samsunggolfmate.beans.ServiceAppPojo;
import com.teewell.samsunggolfmate.beans.WrapperBean;

@TargetApi(Build.VERSION_CODES.GINGERBREAD)
public class UpdateApp {
    protected static final int HASUPDATE = 0;
    private String path = ".masterCourse/apk/";// 下载的apk在sdcard中的位置
    private String fileurl = "http://vm.mastergolf.cn/v0/version/get?package=";
    private String fileName;// 下载后apk的名字
    private String urlStr = "http://vm.mastergolf.cn/v0/version/check?package=";// 下载的url后接文件名
    protected static final int DOWNLOADCOMPLETE = 1;
    protected static final int INSTALAPKFILE = 2;
    protected static final int NO_UPDATE = 3;
    private Activity acticity;
    private DownloadManager dm;
    private File file;
    private long id;
    private Handler updatehandler = new Handler() {
	public void handleMessage(Message msg) {
	    switch (msg.what) {
	    case NO_UPDATE:
	    	ToastUtil.showTips(R.drawable.tips_smile, "已是最新版本！");
	    	break;
	    case HASUPDATE:
		final ServerAppBean appBean = (ServerAppBean) msg.obj;
		DialogUtil.showAskDialog(acticity, "",
			acticity.getString(R.string.update_has_update_ask)
				+ "\n" + appBean.getDescription(),
			new OnClickListener() {

			    @Override
			    public void onClick(DialogInterface dialog,
				    int which) {
				// TODO Auto-generated method stub
				fileName = acticity
					.getString(R.string.app_name)+"_"+appBean.getName();
				Download(fileurl + acticity.getPackageName());
			    }
			});
		break;
	    case DOWNLOADCOMPLETE:
		DialogUtil.showAskDialog(acticity, "", acticity
			.getString(R.string.update_download_complete_ask),
			new OnClickListener() {

			    @Override
			    public void onClick(DialogInterface dialog,
				    int which) {
				// TODO Auto-generated method stub
				instalApk();
			    }
			});
		break;
	    case INSTALAPKFILE:
		DialogUtil.showAskDialog(acticity, "",
			acticity.getString(R.string.update_apk_file_exists),
			new OnClickListener() {

			    @Override
			    public void onClick(DialogInterface dialog,
				    int which) {
				// TODO Auto-generated method stub
				instalApk();
			    }
			});
		break;
	    default:
		break;
	    }
	};
    };
    private BroadcastReceiver receiver = new BroadcastReceiver() {
	@Override
	public void onReceive(Context context, Intent intent) {
	    Message msg = updatehandler.obtainMessage();
	    msg.what = DOWNLOADCOMPLETE;
	    // 这里可以取得下载的id，这样就可以知道哪个文件下载完成了。适用与多个下载任务的监听
	    updatehandler.sendMessage(msg);
	}
    };

    /**
     * 判断是否有更新
     * 
     * @param url
     * @return
     * @throws IOException
     */
    private void checkUpdate(String url) throws IOException {
    	VolleyUitl.getInstence(MyApplication.getContext()).Get(url, new Listener<String>() {

	    @Override
	    public void onResponse(String response) {
		// TODO Auto-generated method stub
		try {
		    ServiceAppPojo appPojo = (ServiceAppPojo) JsonProcessUtil
			    .fromJSON(response, ServiceAppPojo.class);
		    System.out.println(response);
		    if (appPojo.getCode() != WrapperBean.SUCCESS) {
				return;
		    }
		    if (appPojo.getData().getCode() > getVersion()) {
				Message msg = new Message();
				msg.what = HASUPDATE;
				msg.obj = appPojo.getData();
				updatehandler.sendMessage(msg);
		    } else {
		    	if( isShowPromptInfoTheNoUpdate ) {
		    		Message msg = new Message();
					msg.what = NO_UPDATE;
					updatehandler.sendMessage(msg);
		    	}
			}

		} catch (Exception e) {
		    e.printStackTrace();
		}
	    }
	}, new ErrorListener() {

	    @Override
	    public void onErrorResponse(VolleyError error) {
		// TODO Auto-generated method stub

	    }
	},url);
    }
    
    //是否显示没有更新的提示信息
    private boolean isShowPromptInfoTheNoUpdate = false;
    /**
     * 设置当没有更新的时候，是否需要提示信息
     */
    public void setIsShowPromptInfoTheNoUpdate(boolean isShow) {
    	this.isShowPromptInfoTheNoUpdate = isShow;
    }

    /**
     * 获取下个版本VersionName
     * 
     * @return versionName
     */
    private int getVersion() {
	try {
	    // 获取packagemanager的实例
	    PackageManager packageManager = acticity.getPackageManager();
	    // getPackageName()是你当前类的包名，0代表是获取版本信息
	    PackageInfo packInfo = packageManager.getPackageInfo(
		    acticity.getPackageName(), 0);
	    int versioncode = packInfo.versionCode;
	    return versioncode;
	} catch (NumberFormatException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	} catch (Exception e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	return 0;
    }
    
    /**
     * 获取版本名
     */
    public static String getVersionName (){
    	PackageManager packageManager = MyApplication.getContext().getPackageManager();
	    PackageInfo packInfo = null;
		try {
			packInfo = packageManager.getPackageInfo(
					MyApplication.getContext().getPackageName(), 0);
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
	    return packInfo==null?"":packInfo.versionName;
    }

    public void update() {

	new Thread(new Runnable() {

	    @Override
	    public void run() {
		// TODO Auto-generated method stub
		try {
		    checkUpdate(urlStr + acticity.getPackageName());
		} catch (IOException e) {
		    // TODO Auto-generated catch block
		    e.printStackTrace();
		}
	    }
	}).start();
    }

    private void instalApk() {
	Intent intent = new Intent();
	intent.setAction(android.content.Intent.ACTION_VIEW);
	intent.setDataAndType(Uri.fromFile(file),
		"application/vnd.android.package-archive");
	intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	acticity.startActivity(intent);
    }

    public UpdateApp(Activity acticity) {
	super();
	this.acticity = acticity;
    }

    /**
     * 调用系统DownloadManager下载文件
     * 
     * @param url
     */
    private void Download(String url) {
	// TODO Auto-generated method stub
	FileUtils utils = new FileUtils();
	file = utils.getSDFile(path + fileName);
	Log.e("--",
		fileName + "--" + file.getAbsolutePath() + "--"
			+ file.getName() + "--" + file.getPath());
	if (file.exists()) {
	    updatehandler.sendEmptyMessage(INSTALAPKFILE);
	    return;
	} else {
	    dm = (DownloadManager) acticity
		    .getSystemService(Context.DOWNLOAD_SERVICE);
		if (!file.getParentFile().exists()
			) {
		    file.getParentFile().mkdirs();
		}
	}

	DownloadManager.Request request = new DownloadManager.Request(
		Uri.parse(url.toString()));
	request.setAllowedNetworkTypes(Request.NETWORK_MOBILE
		| Request.NETWORK_WIFI);
	request.setAllowedOverRoaming(false);
	// 设置文件类型
	MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
	String mimeString = mimeTypeMap.getMimeTypeFromExtension(MimeTypeMap
		.getFileExtensionFromUrl(url));
	request.setMimeType(mimeString);
	// 在通知栏中显示
	request.setShowRunningNotification(true);
	request.setVisibleInDownloadsUi(true);
	request.setTitle(fileName);
	// sdcard的目录下的download文件夹
	request.setDestinationInExternalPublicDir(path, fileName);
	request.setTitle(fileName);
	id = dm.enqueue(request);
	acticity.registerReceiver(receiver, new IntentFilter(
		DownloadManager.ACTION_DOWNLOAD_COMPLETE));
    }
}
