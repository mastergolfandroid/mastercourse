package com.teewell.samsunggolfmate.utils;

import com.teewell.samsunggolfmate.common.Contexts;
import com.teewell.samsunggolfmate.models.PlayGameModel;

import android.content.Context;
public class DistanceDataUtil{
	public static int[] getDistanceArc(Context context) {
		int[] results = null;
		SharedPreferencesUtils utils = new SharedPreferencesUtils(context);
		String string = utils.getString(PlayGameModel.DISTANCEARC, null);
		if (string == null) {
			results = new int[]{ 240 , 210 , 180 , 150 , 120};
			StringBuffer buffer = new StringBuffer();
			for (int i = 0; i < results.length - 1; i++)
			{
				buffer.append(results[i] + Contexts.STRING_SEPARATOR);
			}
			buffer.append(results[results.length - 1]);
			utils.commitString(PlayGameModel.DISTANCEARC, buffer.toString());
		}else if (!"".equals(string)) {
			String[] strings = string.split(Contexts.STRING_SEPARATOR);
			results = new int[strings.length];
			for (int i = 0; i < results.length; i++) {
				try {
					results[i] = Integer.valueOf(strings[i]).intValue();
				} catch (NumberFormatException e) {
					results[i] = 0;
					e.printStackTrace();
				}
			}
		}
		return results;
	}
}
