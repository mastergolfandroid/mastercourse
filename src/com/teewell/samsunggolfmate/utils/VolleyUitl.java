package com.teewell.samsunggolfmate.utils;

import java.util.HashMap;
import java.util.Map;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

public class VolleyUitl {
	private static final int MAX_IMAGE_CACHE_ENTIRES = 1000;
	private static VolleyUitl instence;
	private RequestQueue mRequestQueue;
	private ImageLoader mImageLoader;
	public static VolleyUitl getInstence(Context context) {
		if (null == instence) {
			instence = new VolleyUitl(context);
		}
		return instence;
	}
	/**
	 * 初始化RequestQueue，ImageLoader
	 * @param context
	 */
	public VolleyUitl(Context context) {
		// TODO Auto-generated constructor stub
		mRequestQueue = Volley.newRequestQueue(context);
        mImageLoader = new ImageLoader(mRequestQueue, new BitmapLruCache(MAX_IMAGE_CACHE_ENTIRES));
	}
	/**
	 * Returns instance of RequestQueue initialized which effectively means
	 * only once.
	 * @return
	 */
	public RequestQueue getRequestQueue() {
        if (mRequestQueue != null) {
            return mRequestQueue;
        } else {
            throw new IllegalStateException("RequestQueue not initialized");
        }
    }
    /**
	 * Returns instance of ImageLoader initialized with {@see FakeImageCache} which effectively means
	 * that no memory caching is used. This is useful for images that you know that will be show
	 * only once.
	 * 
	 * @return
	 */
	public ImageLoader getImageLoader() {
	    if (mImageLoader != null) {
	        return mImageLoader;
	    } else {
	        throw new IllegalStateException("ImageLoader not initialized");
	    }
	}
	/**
	 * 
	 * @param url
	 * @param params
	 * @param listener
	 * @param errorListener
	 * @param tag 用于cancelAll(tag);
	 */
	public void Post(String url,final HashMap<String, String> params,Listener<String> listener,ErrorListener errorListener,Object tag){
    	StringRequest myReq = new StringRequest(Method.POST,
				url,
				listener, errorListener) {
			@Override
			protected Map<String, String> getParams() throws AuthFailureError {
				// TODO Auto-generated method stub
				return params;
			}
			
		};
		if (null!=tag) {
    		myReq.setTag(tag);
		}
		mRequestQueue.add(myReq);
    }
    public void getImage(String url, Listener<Bitmap> listener, int maxWidth, int maxHeight, Config decodeConfig, ErrorListener errorListener, Object tag){
    	ImageRequest request = new ImageRequest(url, listener, maxWidth, maxHeight, decodeConfig, errorListener);
    	if (null!=tag) {
			request.setTag(tag);
		}
    	Log.e("volley", url);
    	mRequestQueue.add(request);
    }
	/**
	 * 
	 * @param url
	 * @param listener
	 * @param errorListener
	 * @param tag 用于cancelAll(tag);
	 */
    public void Post(String url,Listener<String> listener,ErrorListener errorListener,Object tag){
    	StringRequest myReq = new StringRequest(Method.POST,
				url,
				listener, errorListener);
    	if (null!=tag) {
    		myReq.setTag(tag);
		}
		mRequestQueue.add(myReq);
    }
    public void cancelAll(Object tag){
    	mRequestQueue.cancelAll(tag);
    }
    /**
     * 
     * @param url
     * @param listener
     * @param errorListener
     * @param tag 用于cancelAll(tag);
     */
    public void Get(String url,Listener<String> listener,ErrorListener errorListener,String tag){
    	StringRequest myReq = new StringRequest(Method.GET,
				url,
				listener, errorListener);
    	if (null!=tag) {
    		myReq.setTag(tag);
		}
		mRequestQueue.add(myReq);
    }
}
