package com.teewell.samsunggolfmate.utils;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

/**
 * 分享
 */
public class ShareUtil {
	
	/**
	 * 邮箱
	 */
	public static final String MESSAGE_RFC822 = "message/rfc822";
	/**
	 * 普通文本
	 */
	public static final String TEXT_PLAIN = "text/plain";
	/**
	 * 图片
	 */
	public static final String IMAGE_JPEG = "image/jpeg";
	
	
	public interface ShareExtras{
		/**
		 * 发邮件
		 * @param emailRecivers 接收者
		 * @param emailSubject	标题
		 * @param emailBody	邮件内容
		 * @return
		 */
		public Bundle getEmailBundle( String[] emailRecivers, String emailSubject, String emailBody );
		public Bundle getEmailBundle( String emailReciver, String emailSubject, String emailBody );
		/**
		 * 纯文本
		 * @param subject 标题
		 * @param content 内容
		 * @return
		 */
		public Bundle getTxtBundle( String subject, String content );
		/**
		 * 图片内容
		 * @param imageUris 图片uri
		 * @param content 内容
		 * @return
		 */
		public Bundle getImageBundle( Uri[] imageUris, String content );
		public Bundle getImageBundle( Uri imageUri, String content );
	}
	
	public static final ShareExtras SHARE_INTENT = new ShareExtras() {
		
		/**
		 * 发邮件
		 * @param emailRecivers 接收者
		 * @param emailSubject	标题
		 * @param emailBody	邮件内容
		 * @return
		 */
		@Override
		public Bundle getEmailBundle( String[] emailRecivers, String emailSubject, String emailBody ) {
			Bundle extras = new Bundle();
			extras.putStringArray(android.content.Intent.EXTRA_EMAIL, emailRecivers);
			extras.putString(android.content.Intent.EXTRA_SUBJECT, emailSubject);
			extras.putString(android.content.Intent.EXTRA_TEXT, emailBody);
			return extras ;
		}
		/**
		 * 纯文本
		 * @param subject 标题
		 * @param content 内容
		 * @return
		 */
		@Override
		public Bundle getTxtBundle( String subject, String content ) {
			Bundle extras = new Bundle();
			extras.putString(Intent.EXTRA_SUBJECT, subject);
			extras.putString(Intent.EXTRA_TEXT, content);
			return extras;
		}
		/**
		 * 图片内容
		 * @param imageUris 图片uri
		 * @param content 内容
		 * @return
		 */
		@Override
		public Bundle getImageBundle( Uri[] imageUris, String content ) {
			Bundle extras = new Bundle();
			extras.putParcelableArray(Intent.EXTRA_STREAM, imageUris);
			extras.putString(Intent.EXTRA_TEXT, content);
			return extras;
		}
		@Override
		public Bundle getEmailBundle(String emailReciver, String emailSubject,
				String emailBody) {
			Bundle extras = new Bundle();
			extras.putString(android.content.Intent.EXTRA_EMAIL, emailReciver);
			extras.putString(android.content.Intent.EXTRA_SUBJECT, emailSubject);
			extras.putString(android.content.Intent.EXTRA_TEXT, emailBody);
			return extras ;
		}
		@Override
		public Bundle getImageBundle(Uri imageUri, String content) {
			Bundle extras = new Bundle();
			extras.putParcelable(Intent.EXTRA_STREAM, imageUri);
			extras.putString(Intent.EXTRA_TEXT, content);
			return extras;
		}
	};
}