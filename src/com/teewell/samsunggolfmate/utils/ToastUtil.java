/**
 * 
 */ 
package com.teewell.samsunggolfmate.utils; 

import android.os.Build;

import com.teewell.samsunggolfmate.activities.MyApplication;

/** 
 * @author 程明炎 E-mail: 345021109@qq.com
 * @version 创建时间：2014年7月13日 下午3:33:54 
 * 类说明 
 */
/**
 * @author sxx
 *
 */
public class ToastUtil {
	private static TipsToast tipsToast;
	/**
	 * 自定义taost
	 * 
	 * @param iconResId
	 *            图片
	 * @param msgResId
	 *            提示文字
	 */
	public static void showTips(int iconResId, String tips) {
		if (tipsToast != null) {
			if (Build.VERSION.SDK_INT < Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
				tipsToast.cancel();
			}
		} else {
			tipsToast = TipsToast.makeText(MyApplication.getContext(),
					tips, TipsToast.LENGTH_SHORT);
		}
		tipsToast.show();
		tipsToast.setIcon(iconResId);
		tipsToast.setText(tips);
	}
}
 