package com.teewell.samsunggolfmate.utils;

import android.widget.ImageView;
import android.widget.TextView;

/**
 * Displays bitmap in {@link ImageView}. Must be called on UI thread.
 * 
 * @author Sergey Tarasevich (nostra13[at]gmail[dot]com)
 * @see ImageLoadingListener
 */
final class DisplayTextViewTask implements Runnable {

	private final String bitmap;
	private final TextView imageView;

	public DisplayTextViewTask(String bitmap, TextView imageView) {
		this.bitmap = bitmap;
		this.imageView = imageView;
	}

	public void run() {
		imageView.setText(bitmap);
	}
}
