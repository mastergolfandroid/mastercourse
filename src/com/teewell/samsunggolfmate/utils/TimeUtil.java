package com.teewell.samsunggolfmate.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import android.util.Log;

public class TimeUtil {
//	DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
//	private final String //中国时区的信息
//    DEFAULT_TIME_ZONE_INFORMATION = {-480};
//	private SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private static final String TAG = TimeUtil.class.getName();
	
	public static final String LOCAL_TIME_PATTERN_STRING = "yyyy-MM-dd";
	public static final String LOCAL_TIME_WITHOUT_YEAR_PATTERN_STRING = "MM-dd";

	public static final String LOCAL_DETAIL_TIME_PATTERN_STRING = "yyyy-MM-dd HH:mm";
	public static final String LOCAL_DETAIL_TIME_WITHOUT_YEAR_PATTERN_STRING = "MM-dd HH:mm";
	public static final long  ONE_DAY_TIMES = 1000 * 60 * 60 * 24;
	/**
	 * 
	 */
	public static String[] getDateByChangDay(){
		String[] time = new String[5];
		Calendar calendar = Calendar.getInstance();
//		calendar.setTime(new Date());
		calendar.add(Calendar.DAY_OF_YEAR, -1);
//		cal.getTime();
		time[0] = String.valueOf(calendar.get(Calendar.YEAR));
		time[1] = String.valueOf(calendar.get(Calendar.MONTH));
		time[2] = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
		time[3] = String.valueOf(calendar.get(Calendar.HOUR_OF_DAY));
		time[4] = String.valueOf(calendar.get(Calendar.MINUTE));
		return time;
	}
	
	public static String[] getDate(){
		String[] time = new String[5];
		Calendar calendar = Calendar.getInstance();
		time[0] = String.valueOf(calendar.get(Calendar.YEAR));
		time[1] = String.valueOf(calendar.get(Calendar.MONTH));
		time[2] = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
		time[3] = String.valueOf(calendar.get(Calendar.HOUR_OF_DAY));
		time[4] = String.valueOf(calendar.get(Calendar.MINUTE));
		return time;
	}
	public static String[] getHourByEveryday(){
		String[] time = new String[24];
		for (int i = 0; i < time.length; i++) {
			time[i] = String.valueOf(i);
			if (i<10) {
				time[i] = String.valueOf(0)+time[i];
			}
		}
		return time;
	}
	public static String[] getMinuteByEveryHour(){
		String[] time = new String[60];
		for (int i = 0; i < time.length; i++) {
			time[i] = String.valueOf(i);
			if (i<10) {
				time[i] = String.valueOf(0)+time[i];
			}
		}
		return time;
	}

	public static long dateToUtcTime(Date date){
		//将本地当前时间转换成UTC国际标准时间的毫秒形式
        if(date != null){   
        	Calendar calendar = Calendar.getInstance();
    		calendar.setTime(date);
    		return calendar.getTimeInMillis();
        }else{
        	return 0;
        }
	}

	/**
	 * @param utcTime
	 * @param localTimePatten
	 * @return
	 */
	public static String local2utc(String localTime) { 
		String utcTimeString = "";
		if (localTime!=null && localTime.length() > 0) {
			SimpleDateFormat localFormater = new SimpleDateFormat(LOCAL_TIME_PATTERN_STRING); 
			localFormater.setTimeZone(TimeZone.getDefault());
			Date date;
			try{
				date = localFormater.parse(localTime);
			}catch (Exception e) {
				// TODO: handle exception
				Log.e(TAG, "local time:" + localTime + " is invalid time string");
				return "";
			}
			long utcTimes = dateToUtcTime(date);
			utcTimeString = String.valueOf(utcTimes);
		}
		return utcTimeString; 
	}
	
	/**
	 * @param utcTime
	 * @param localTimePatten
	 * @return
	 */
	public static long local2utcTimes(String localTime) { 
		long utcTimes = 0;
		if (localTime!=null && localTime.length() > 0) {
			SimpleDateFormat localFormater = new SimpleDateFormat(LOCAL_TIME_PATTERN_STRING); 
			localFormater.setTimeZone(TimeZone.getDefault());
			Date date;
			try{
				date = localFormater.parse(localTime);
			}catch (Exception e) {
				// TODO: handle exception
				Log.e(TAG, "local time:" + localTime + " is invalid time string");
				return utcTimes;
			}
			utcTimes = dateToUtcTime(date);
		}
		return utcTimes; 
	}
	/**
	 * @description:将utc时间(毫秒)数转换为date时间类型
	 * @date: 2011-10-11下午03:52:45
	 * @author: 崔松
	 */
	public static Date utcMsTimeToDate(long utcTime){
		Date resultDate = null;
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(utcTime);
		resultDate = calendar.getTime();
		return resultDate;
	}

	public static int getCurrentYear() { 
		Calendar calendar = Calendar.getInstance();
		int curYear = calendar.get(Calendar.YEAR);
		
		return curYear;
	}

	public static String getStringOneDayBefore(String dateString) {
		String localTime = "";
		if (dateString!=null && dateString.length() > 0) {
			SimpleDateFormat localFormater = new SimpleDateFormat(LOCAL_TIME_PATTERN_STRING); 
			Date date;
			try{
				date = localFormater.parse(dateString);
			}catch (Exception e) {
				// TODO: handle exception
				Log.e(TAG, "local time:" + dateString + " is invalid time string");
				return "";
			}
			long Times = date.getTime();
			
			Times -= ONE_DAY_TIMES;
			
			Date newDate = new Date(Times);
			localTime = localFormater.format(newDate);
		}
		return localTime;
	}

	public static String getStringOneDayAfter(String dateString) {
		String localTime = "";
		if (dateString != null && dateString.length() > 0) {
			SimpleDateFormat localFormater = new SimpleDateFormat(LOCAL_TIME_PATTERN_STRING); 
			Date date;
			try{
				date = localFormater.parse(dateString);
			}catch (Exception e) {
				// TODO: handle exception
				Log.e(TAG, "local time:" + dateString + " is invalid time string");
				return "";
			}
			long Times = date.getTime();
			
			Times += ONE_DAY_TIMES;
			
			Date newDate = new Date(Times);
			localTime = localFormater.format(newDate);
		}
		return localTime;
	}

	public static int getDayNumByYearMonth(String year, String month) { 

		int monthInt = Integer.parseInt(month);
		int yearInt = Integer.parseInt(year);
		switch (monthInt) {
		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12:
			return 31;
		case 2:
			if ((yearInt % 400) == 0 ||
				(((yearInt % 100) != 0) && ((yearInt %4) == 0))){
				return 29;
			}else{
				return 28;
			}
		default:
			return 30;
		}
	}


	/**将long型时间转为2012-11-25 12:12:12型
	 * @param utcTime
	 * @param localTimePatten
	 * @return
	 */
	public static String utc2LocalWithoutYear(long utcTime) { 
		String localTime = "";
		if (utcTime > 0) {
			Date localDate = utcMsTimeToDate(utcTime);
			int curYear = getCurrentYear();
			
			SimpleDateFormat localFormater;
			int year = localDate.getYear() + 1900;
			if (year == curYear) {
				localFormater = new SimpleDateFormat(LOCAL_TIME_WITHOUT_YEAR_PATTERN_STRING); 
			}else{
				localFormater = new SimpleDateFormat(LOCAL_TIME_PATTERN_STRING); 
			}
			
			localFormater.setTimeZone(TimeZone.getDefault()); 
			localTime = localFormater.format(localDate); 
		}
		return localTime; 
	}

	/**将long型时间转为2012-11-25 12:12:12型
	 * @param utcTime
	 * @param localTimePatten
	 * @return
	 */
	public static String utc2LocalTimeWithoutYear(long utcTime) { 
		String localTime = "";
		if (utcTime > 0) {
			Date localDate = utcMsTimeToDate(utcTime);
			int curYear = getCurrentYear();
			
			SimpleDateFormat localFormater;
			int year = localDate.getYear() + 1900;
			if (year == curYear) {
				localFormater = new SimpleDateFormat(LOCAL_DETAIL_TIME_WITHOUT_YEAR_PATTERN_STRING); 
			}else{
				localFormater = new SimpleDateFormat(LOCAL_DETAIL_TIME_PATTERN_STRING); 
			}
			
			localFormater.setTimeZone(TimeZone.getDefault()); 
			localTime = localFormater.format(localDate); 
		}
		return localTime; 
	}
	/**将2012-11-25 12:12型转为数组
	 * @param dateString
	 * @return
	 */
	public static String[] getYearMonthDayHourMinute(String  dateString) { 
		String localTime[] = null;
		if (dateString != null && dateString.length() > 0) {
			localTime = new String[5];
			String[] time = dateString.split(" "); 
			String[] time1 = time[0].split("-"); 
			String[] time2 = time[1].split(":"); 
			localTime[0] = time1[0];
			localTime[1] = time1[1];
			localTime[2] = time1[2];
			localTime[3] = time2[0];
			localTime[4] = time2[1];
		}
		return localTime; 
	}

	public static String utc2Local(long utcTime) { 
		String localTime = "";
		if (utcTime > 0) {
			Date localDate = utcMsTimeToDate(utcTime);
			SimpleDateFormat localFormater = new SimpleDateFormat(LOCAL_TIME_PATTERN_STRING); 
			localFormater.setTimeZone(TimeZone.getDefault()); 
			localTime = localFormater.format(localDate); 
		}
		return localTime; 
	}

	/**
	 * 判断时间A是否在时间B之前
	 * 
	 * @param dateStart
	 *            时间A
	 * @param dateEnd
	 *            时间B
	 * @return 返回true，表示时间A在时间B之前
	 * @throws ParseException
	 */
	public static boolean isDateOk ( String dateStart, String dateEnd )
			throws ParseException
	{
		System.out.println("start:" + dateStart);
		System.out.println("end:" + dateEnd);

		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");//定义时间格式
		Date start = df.parse(dateStart);
		Date end = df.parse(dateEnd);
		return start.before(end);
	}

	/**
	 * 将字符串转换成时间，如果时间字符串不能按指定格式转换，则抛出ParseException异常
	 * 
	 * @param dateString
	 *            时间字符串
	 * @param format
	 *            转换的格式，如果为空，则默认格式为yyyy-MM-dd
	 * @return Date对象
	 * @throws ParseException
	 */
	public static Date stringToDate ( String dateString, String format )
			throws ParseException
	{
		SimpleDateFormat df = null;
		if (format == null)
		{
			df = new SimpleDateFormat("yyyy-MM-dd");
		}
		else
		{
			df = new SimpleDateFormat(format);
		}
		Date date = df.parse(dateString);
		return date;
	}
}
