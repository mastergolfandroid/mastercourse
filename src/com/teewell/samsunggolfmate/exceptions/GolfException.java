package com.teewell.samsunggolfmate.exceptions;

import java.text.MessageFormat;
import java.util.Locale;

public class GolfException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5719647889753237554L;
	private Class<?> rise;
	private String name;
	private Object[] arguments;
	private int code;
	public GolfException(Class<?> rise,int code,String name) {
		this(rise,code,name,null);
	}
	public GolfException(Class<?> rise,int code,String name,Object[] arguments) {
		this.rise = rise;
		this.code = code;
		this.name = name;
		this.arguments = arguments;
	}
	
	public String toString() {
		return "{\"code\":" + code + ",\"info\":\"" + getMessage() + "\"}";
	}
	public int getCode() {
		return code;
	}
	public String getName(){
		return this.name;
	}
	/**
	 * @return the rise
	 */
	public Class<?> getRise() {
		return rise;
	}

	public String getMessage() {
		String rtv = null;
		rtv = rise.getName()+ "." + name;
		for(int i = 0 ; arguments != null && i < arguments.length; i++) {
			rtv += i + ":" + arguments[i].toString();
		}
		return rtv;
	}
	public String getLocalizedMessage(Locale locale) {
		
		String m = null;
		String rtv = null;
		try {
			m = name;
			if (m != null && m.length() > 0 && arguments != null){
				MessageFormat form = new MessageFormat(m);
				rtv = form.format(arguments);
			} else if (m != null && m.length() > 0 && arguments == null){
				rtv = m;
			}
		} catch(Throwable t) {
			rtv = name;//rise.getName()+ "." + 
			for(int i = 0 ; arguments != null && i < arguments.length; i++) {
				rtv += i + ":" + arguments[i].toString();
			}
		}
		return rtv;
	}
}
