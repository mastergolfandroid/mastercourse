package com.teewell.samsunggolfmate.activities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.widget.Toast;

import com.baidu.mobstat.StatService;
import com.teewell.samsunggolfmate.utils.ActivityManagerUtil;

/**
 * @author Administrator 程明炎
 */
public class ParentActivity extends Activity {
	/**
	 * 消息提示栏，通过统一的消息提示栏可以防止多次弹出toast
	 */
	private Toast toast = null;
	private HomeKeyEventBroadCastReceiver receiver;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		ActivityManagerUtil.getScreenManager().pushActivity(this);
		receiver = new HomeKeyEventBroadCastReceiver();
		super.onCreate(savedInstanceState);
	}

	@Override
	protected void onResume() {
		super.onResume();
		registerReceiver(receiver, new IntentFilter(Intent.ACTION_CLOSE_SYSTEM_DIALOGS));
		/**
		 * 页面起始（注意： 每个Activity中都需要添加，如果有继承的父Activity中已经添加了该调用，那么子Activity中务必不能添加）
		 * 如果该FragmentActivity包含了几个全页面的fragment，那么可以在fragment里面加入就可以了，这里可以不加入。如果不加入将不会记录该Activity页面。
		 */
		StatService.onResume(this);
	}

	@Override
	protected void onPause() {
		super.onPause();
		 unregisterReceiver(receiver);
		 /**
			 * 页面结束（注意： 每个Activity中都需要添加，如果有继承的父Activity中已经添加了该调用，那么子Activity中务必不能添加）
			 * 如果该FragmentActivity包含了几个全页面的fragment，那么可以在fragment里面加入就可以了，这里可以不加入。如果不加入将不会记录该Activity页面。
			 */
			StatService.onPause(this);
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		ActivityManagerUtil.getScreenManager().popActivity(this);
	}
	 class HomeKeyEventBroadCastReceiver extends BroadcastReceiver {

		   static final String SYSTEM_REASON = "reason";
		   static final String SYSTEM_HOME_KEY = "homekey";//home key
		   static final String SYSTEM_RECENT_APPS = "recentapps";//long home key
		   
		   @Override
		   public void onReceive(Context context, Intent intent) {
		    String action = intent.getAction();
		    if (action.equals(Intent.ACTION_CLOSE_SYSTEM_DIALOGS)) {
		     String reason = intent.getStringExtra(SYSTEM_REASON);
		     if (reason != null) {
		      if (reason.equals(SYSTEM_HOME_KEY)) {
		       // home key处理点
		      } else if (reason.equals(SYSTEM_RECENT_APPS)) {
		       // long home key处理点
		      }
		     }
		    }
		   }
		  }
	/**
	 * 弹出提示对话框
	 * 
	 * @param msg
	 *            指定要提示的内容
	 */
	public void showToast(String msg) {

		// 确保toast唯一
		if (toast == null) {
			toast = Toast.makeText(getApplicationContext(), msg,
					Toast.LENGTH_SHORT);
		} else {
			toast.setText(msg);
		}
		toast.show();
	}
}
