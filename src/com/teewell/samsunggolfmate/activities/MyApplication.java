package com.teewell.samsunggolfmate.activities;

import android.app.Application;
import android.content.Context;
import android.widget.Toast;
import cn.sharesdk.framework.ShareSDK;

import com.baidu.mapapi.BMapManager;
import com.baidu.mapapi.MKGeneralListener;
import com.baidu.mapapi.map.MKEvent;

public class MyApplication extends Application {
	public static MyApplication mInstance = null;
	private static Context context;

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		context = getApplicationContext();
		mInstance = this;
		
	}
	/**
	 * 获得ApplicationContext
	 * @return
	 */
	public static Context getContext() { 
        return context; 
    } 
}
