package com.teewell.samsunggolfmate.models.weather;

import org.apache.http.ParseException;

import com.android.volley.Response.Listener;
import com.teewell.samsunggolfmate.beans.WeatherInfoBean;
import com.teewell.samsunggolfmate.views.CourseListCellWrapper;

public class WeatherImageResponseListener implements Listener<String>{
	@SuppressWarnings("unused")
	private static final String TAG = WeatherImageResponseListener.class.getName();

	private CourseListCellWrapper cellView;
	private String city;
	
	public WeatherImageResponseListener(String cityString, CourseListCellWrapper cellView){
		this.cellView = cellView; // default type
		this.city = cityString;
	}
	
	@Override
	public void onResponse(String response) {
		// TODO Auto-generated method stub
		try {
			WeatherInfoModel model = WeatherInfoModel.getInstance(null);
			model.submitWeatherImage(city, response);
			
			//change the cellview
			WeatherInfoBean bean = model.getWeatherInfoBean(city);
			if(null == bean){
				return;
			}
			cellView.courseWeather.setImageBitmap(bean.getBitmap());
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			
//			e.printStackTrace();
		}	
	}


}
