package com.teewell.samsunggolfmate.beans;


/**
 * 网络请求返回数据的封装类
 */
public class WrapperBean {
	
	@SuppressWarnings("unused")
	private static final String TAG = WrapperBean.class.getName();
	
	/**
	 * 成功码
	 */
	public static final int SUCCESS = 10000;
	public static final int VERIFICATION_FAILED = 40401;
	/**
	 * 状态
	 */
	private String status ;
	/**
	 * 状态码
	 */
	private int code;
	/**
	 * 错误信息
	 */
	private String message;
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	/**
	 * 错误信息
	 * @return status failure return error info .
	 */
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
}
