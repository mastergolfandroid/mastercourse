package com.teewell.samsunggolfmate.beans;

import java.io.Serializable;


/**
 * @Description:用户信息数据对象(和view交互)
 * @Author: xsui
 * @Title: UserInfoObject.java
 * @Package: cn.teewell.userInfo.data
 * @Date: 2013-4-18 上午10:16:20
 * @Version: 1.0 Copyright: Copyright (c) 2012
 */
public class UserInfoObject implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7225450515640985583L;
	private String userName;
	private Integer userSex;
	private String userBirthday;
	
	private String mobileNumber;
	private String userEmail;
	private String userSign;

	
	public Integer getUserSex() {
		return userSex;
	}

	public void setUserSex(Integer userSex) {
		this.userSex = userSex;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the userBirthday
	 */
	public String getUserBirthday() {
		return userBirthday;
	}

	/**
	 * @param userBirthday the userBirthday to set
	 */
	public void setUserBirthday(String userBirthday) {
		this.userBirthday = userBirthday;
	}

	/**
	 * @return the mobileNumber
	 */
	public String getMobileNumber() {
		return mobileNumber;
	}

	/**
	 * @param mobileNumber the mobileNumber to set
	 */
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	/**
	 * @return the userEmail
	 */
	public String getUserEmail() {
		return userEmail;
	}

	/**
	 * @param userEmail the userEmail to set
	 */
	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	/**
	 * @return the userSign
	 */
	public String getUserSign() {
		return userSign;
	}

	/**
	 * @param userSign the userSign to set
	 */
	public void setUserSign(String userSign) {
		this.userSign = userSign;
	}
	

}
