package com.teewell.samsunggolfmate.beans;

/**
 * @author huanghua
 *
 */
public class MapMarkeVo {

    /**
     * С ��markeͼ��
     */
    public static final int STATE_1 = 1;
    /**
     * ɾ��markeͼ��
     */
    public static final int STATE_2 = 2;
    /**
     * ɾ���
     */
    public static final int STATE_3 = 3;
    /**
     * ���϶��Ĺ�̣��Ŵ�markeͼ��
     */
    public static final int STATE_4 = 4;
    private int id = -1;
    private int holeId;
    private int temp_holeId;
    private float x;
    private float y;
    /**
     * 1 : δ�Ķ�  2 ��XY�Ķ���3 ��ɾ��ġ� 4:���϶��Ĺ��
     */
    private int state;//1 : δ�Ķ�  2 ��XY�Ķ���3 ��ɾ��ġ� 4:���϶��Ĺ��

    /**
     * @return the state
     */
    public int getState() {
        return state;
    }

    /**
     * @param state the state to set
     */
    public void setState(int state) {
        this.state = state;
    }

    /**
     * @return the temp_holeId
     */
    public int getTemp_holeId() {
        return temp_holeId;
    }

    /**
     * @param tempHoleId the temp_holeId to set
     */
    public void setTemp_holeId(int tempHoleId) {
        temp_holeId = tempHoleId;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }
    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }
    /**
     * @return the holeId
     */
    public int getHoleId() {
        return holeId;
    }
    /**
     * @param holeId the holeId to set
     */
    public void setHoleId(int holeId) {
        this.holeId = holeId;
    }
    /**
     * @return the x
     */
    public float getX() {
        return x;
    }
    /**
     * @param x the x to set
     */
    public void setX(float x) {
        this.x = x;
    }
    /**
     * @return the y
     */
    public float getY() {
        return y;
    }
    /**
     * @param y the y to set
     */
    public void setY(float y) {
        this.y = y;
    }
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "<MapMarkeVo>> [ id : " + id + " ] [ holeId : " + holeId + " ] [ state : " + state  + " ] [ x : " + x  + " ] [ y : " + y + " ]" ;
    }
    
    
}
