/**
 * 
 */
package com.teewell.samsunggolfmate.beans;

import java.io.Serializable;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;



/**
 * @Description
 * @Author 谭坚
 * @Title SubCoursesObject.java   
 * @Package cn.teewell.golf.data 
 * @Date 2012-5-9 下午2:05:14   
 * @Version 1.0  
 * Copyright: Copyright (c)  2012  
 * Company: teewell    
 */
public class SubCoursesObject implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	private Integer fairwayCount;
	private String  subCoursesName;
	private ArrayList<FairwayObject> fairwayList;
	/**
	 * 返回子球场id
	 * @return
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置子球场id
	 * @param id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * 返回子球场总洞数
	 * @return
	 */
	public Integer getFairwayCount() {
		return fairwayCount;
	}
	/**
	 * 设置子球场总洞数
	 * @param fairwayCount
	 */
	public void setFairwayCount(Integer fairwayCount) {
		this.fairwayCount = fairwayCount;
	}
	/**
	 * 返回子球场名
	 * @return
	 */
	public String getSubCoursesName() {
		return subCoursesName;
	}
	/**
	 * 设置子球场名
	 * @param subCoursesName
	 */
	public void setSubCoursesName(String subCoursesName) {
		this.subCoursesName = subCoursesName;
	}
	
	public ArrayList<FairwayObject> getFairwayList() {
		return fairwayList;
	}
	public void setFairwayList(ArrayList<FairwayObject> fairwayList) {
		this.fairwayList = fairwayList;
	}
	public static SubCoursesObject jsonSubCourseObject(JSONObject jsonObject){
		SubCoursesObject object = new SubCoursesObject();
		object.setId(jsonObject.optInt("subCoursesID"));
		object.setSubCoursesName(jsonObject.optString("subCoursesName"));
		ArrayList<FairwayObject> fairwayObjects = new ArrayList<FairwayObject>();
		JSONArray fairwayList = jsonObject.optJSONArray("fairwayList");
		for (int i = 0; i < fairwayList.length(); i++) {
			fairwayObjects.add(FairwayObject.jsonFairwayObject(fairwayList.optJSONObject(i)));
		}
		object.setFairwayCount(fairwayObjects.size());
		object.setFairwayList(fairwayObjects);
		return object;
	}
}
