package com.teewell.samsunggolfmate.beans;

import java.util.Date;

/**  
 * @Project teewell.ClubCourses
 * @package cn.teewell.golf.bean
 * @title CoursesInfoBean.java 
 * @Description 俱乐部（球会、公司级的）介绍信息
 * @author DarkWarlords
 * @date 2012-9-25 下午6:09:42
 * @Copyright Copyright(C) 2012-9-25
 * @version 1.0.0
 */
public class CoursesInfoBean {

	private String introduction;
	private String detailIntro;
	private Integer countHoles;
	private Integer totalArea;
	private Integer totalLength;
	private Integer totalPar;
	private String fairGrass;
	private String greenGrass;
	private String designer;
	private String website;
	private Date openingTime;
	
	private String addressInfo;
	private String linkman;
	private String mobilePhone;
	private String telphone;
	private String fax;
	private String email;
	private String zip;
	
	public String getIntroduction() {
		return introduction;
	}
	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}
	public String getDetailIntro() {
		return detailIntro;
	}
	public void setDetailIntro(String detailIntro) {
		this.detailIntro = detailIntro;
	}
	public Integer getCountHoles() {
		return countHoles;
	}
	public void setCountHoles(Integer countHoles) {
		this.countHoles = countHoles;
	}
	public Integer getTotalArea() {
		return totalArea;
	}
	public void setTotalArea(Integer totalArea) {
		this.totalArea = totalArea;
	}
	public Integer getTotalLength() {
		return totalLength;
	}
	public void setTotalLength(Integer totalLength) {
		this.totalLength = totalLength;
	}
	public Integer getTotalPar() {
		return totalPar;
	}
	public void setTotalPar(Integer totalPar) {
		this.totalPar = totalPar;
	}
	public String getFairGrass() {
		return fairGrass;
	}
	public void setFairGrass(String fairGrass) {
		this.fairGrass = fairGrass;
	}
	public String getGreenGrass() {
		return greenGrass;
	}
	public void setGreenGrass(String greenGrass) {
		this.greenGrass = greenGrass;
	}
	public String getDesigner() {
		return designer;
	}
	public void setDesigner(String designer) {
		this.designer = designer;
	}
	public String getWebsite() {
		return website;
	}
	public void setWebsite(String website) {
		this.website = website;
	}
	public Date getOpeningTime() {
		return openingTime;
	}
	public void setOpeningTime(Date openingTime) {
		this.openingTime = openingTime;
	}
	
	public String getAddressInfo() {
		return addressInfo;
	}
	public void setAddressInfo(String addressInfo) {
		this.addressInfo = addressInfo;
	}
	public String getLinkman() {
		return linkman;
	}
	public void setLinkman(String linkman) {
		this.linkman = linkman;
	}
	public String getMobilePhone() {
		return mobilePhone;
	}
	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}
	public String getTelphone() {
		return telphone;
	}
	public void setTelphone(String telphone) {
		this.telphone = telphone;
	}
	public String getFax() {
		return fax;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
}
